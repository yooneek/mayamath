﻿namespace MayaMath.Exception
{
    public class InvalidCalculationTypeException: System.Exception
    {
        public InvalidCalculationTypeException(): base("Invalid Calculation Type")
        {
        }
    }
}