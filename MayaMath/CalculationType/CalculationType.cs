﻿namespace MayaMath.CalculationType
{
    public abstract class CalculationType
    {
        public MultiplicationTable MultiplicationTable { get; set; }

        public abstract MathProblem GenerateMathProblem();

        public override bool Equals(object? obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            CalculationType comparedObject = (CalculationType) obj;

            return MultiplicationTable.Value.Equals(comparedObject.MultiplicationTable.Value);
        }
    }
}