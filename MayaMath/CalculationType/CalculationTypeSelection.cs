﻿using System.Collections.Generic;

namespace MayaMath.CalculationType
{
    public class CalculationTypeSelection
    {
        public List<CalculationType> Selection { get; }

        public CalculationTypeSelection()
        {
            Selection = new List<CalculationType>();
        }

        public void AddCalculationType(CalculationType calculationType)
        {
            if (!SelectionContainsCalculationType(calculationType))
            {
                Selection.Add(calculationType);
            }
        }

        public void RemoveCalculationType(CalculationType calculationType)
        {
            if (SelectionContainsCalculationType(calculationType))
            {
                Selection.Remove(calculationType);
            }
        }

        private bool SelectionContainsCalculationType(CalculationType calculationType)
        {
            for (int i = 0; i < Selection.Count; i++)
            {
                if (calculationType.Equals(Selection[i]))
                {
                    return true;
                }
            }

            return false;
        }
    }
}