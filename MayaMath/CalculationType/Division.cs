﻿using System;

namespace MayaMath.CalculationType
{
    public sealed class Division : CalculationType
    {
        private readonly char _operationSign;
        public Division(MultiplicationTable multiplicationTable)
        {
            MultiplicationTable = multiplicationTable;
            _operationSign = ':';
        }

        public override MathProblem GenerateMathProblem()
        {
            Random random = new Random();
            int rightTerm = (int)MultiplicationTable.Value;
            int solution = random.Next(1, 10);
            int leftTerm = rightTerm * solution;

            return new MathProblem(leftTerm, rightTerm, solution, _operationSign);
        }
    }
}