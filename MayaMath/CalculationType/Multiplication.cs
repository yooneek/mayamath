﻿using System;

namespace MayaMath.CalculationType
{
    public sealed class Multiplication : CalculationType
    {
        private readonly char _operationSign;

        public Multiplication(MultiplicationTable multiplicationTable)
        {
            MultiplicationTable = multiplicationTable;
            _operationSign = '*';
        }

        public override MathProblem GenerateMathProblem()
        {
            Random random = new Random();
            int rightTerm = random.Next(1, 10);
            int leftTerm = (int)MultiplicationTable.Value;
            int solution = leftTerm * rightTerm;

            return new MathProblem(leftTerm, rightTerm, solution, _operationSign);
        }
    }
}