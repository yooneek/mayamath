﻿using MayaMath.CalculationType;

namespace MayaMath
{
    public class MathProblem
    {
        public int LeftTerm { get; }
        public int RightTerm { get; }
        public int Solution { get; }
        public char OperationSign { get; }

        public MathProblem(int leftTerm, int rightTerm, int solution, char operationSign)
        {
            LeftTerm = leftTerm;
            RightTerm = rightTerm;
            Solution = solution;
            OperationSign = operationSign;
        }

        public override string ToString()
        {
            return LeftTerm.ToString() + " " + OperationSign.ToString() + " " + RightTerm.ToString() + " = " +
                   Solution.ToString();
        }

    }
}