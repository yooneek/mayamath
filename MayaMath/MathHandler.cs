﻿using System;
using System.Collections.Generic;
using MayaMath.CalculationType;

namespace MayaMath
{
    public class MathHandler
    {
        public CalculationTypeSelection Selection { get; }
        private List<MathProblem> MathProblemSelection { get; set; }

        public MathHandler()
        {
            Selection = new CalculationTypeSelection();
            MathProblemSelection = new List<MathProblem>();
        }

        public void AddCalculationType(CalculationType.CalculationType calculationType)
        {
            Selection.AddCalculationType(calculationType);
        }

        public void RemoveCalculationType(CalculationType.CalculationType calculationType)
        {
            Selection.RemoveCalculationType(calculationType);
        }

        public List<MathProblem> GetRandomMathProblems(int count)
        {
            Random random = new Random();

            for (int i = MathProblemSelection.Count; i < count; i++)
            {
                int randomIndex = random.Next(0, Selection.Selection.Count - 1);
                MathProblemSelection.Add(Selection.Selection[randomIndex].GenerateMathProblem());
            }

            return MathProblemSelection;
        }

        public bool SolveMathProblem(MathProblem mathProblem, int solution)
        {
            if (mathProblem.Solution == solution)
            {
                MathProblemSelection.Remove(mathProblem);
                return true;
            }

            return false;
        }
    }
}