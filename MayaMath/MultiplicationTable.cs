﻿using MayaMath.Enum;

namespace MayaMath
{
    public class MultiplicationTable
    {
        public MultiplicationTableValue Value { get; private set; }

        public MultiplicationTable(MultiplicationTableValue value)
        {
            Value = value;
        }
    }
}