using MayaMath;
using MayaMath.CalculationType;
using MayaMath.Enum;
using NUnit.Framework;

namespace Test
{
    public class CalculationTypeSelectionTest
    {
        [Test]
        public void TestAddCalculationType()
        {
            CalculationTypeSelection selection = new CalculationTypeSelection();

            MultiplicationTable table5 = new MultiplicationTable(MultiplicationTableValue.Five);
            Division divisionTestA = new Division(table5);
            Division divisionTestB = new Division(table5);

            selection.AddCalculationType(divisionTestA);
            Assert.AreEqual(1, selection.Selection.Count);

            selection.AddCalculationType(divisionTestB);
            Assert.AreEqual(1, selection.Selection.Count);

            MultiplicationTable table6 = new MultiplicationTable(MultiplicationTableValue.Six);
            Division divisionTestC = new Division(table6);

            selection.AddCalculationType(divisionTestC);
            Assert.AreEqual(2, selection.Selection.Count);

            Multiplication multiplicationTestA = new Multiplication(table6);
            selection.AddCalculationType(multiplicationTestA);

            Assert.AreEqual(3, selection.Selection.Count);
        }

        [Test]
        public void TestRemoveCalculationType()
        {
            CalculationTypeSelection selection = new CalculationTypeSelection();

            MultiplicationTable table5 = new MultiplicationTable(MultiplicationTableValue.Five);
            MultiplicationTable table6 = new MultiplicationTable(MultiplicationTableValue.Six);
            Division divisionTestA = new Division(table5);
            Division divisionTestB = new Division(table6);

            selection.AddCalculationType(divisionTestA);
            Assert.AreEqual(1, selection.Selection.Count);

            selection.AddCalculationType(divisionTestB);
            Assert.AreEqual(2, selection.Selection.Count);

            selection.RemoveCalculationType(divisionTestB);
            Assert.AreEqual(1, selection.Selection.Count);

            Multiplication multiplicationTestA = new Multiplication(table5);
            selection.RemoveCalculationType(multiplicationTestA);
            Assert.AreEqual(1, selection.Selection.Count);
        }
    }
}