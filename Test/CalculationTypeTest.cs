
using MayaMath;
using MayaMath.CalculationType;
using MayaMath.Enum;
using NUnit.Framework;

namespace Test
{
    public class DivisionTest
    {
        [Test]
        public void TestDivision()
        {
            MultiplicationTable table = new MultiplicationTable(MultiplicationTableValue.Five);
            Division division = new Division(table);
            MathProblem mathProblem = division.GenerateMathProblem();
            int expectedSolution = mathProblem.LeftTerm / mathProblem.RightTerm;
            Assert.AreEqual(expectedSolution, mathProblem.Solution);
            Assert.AreEqual(':', mathProblem.OperationSign);
        }

        [Test]
        public void TestMultiplication()
        {
            MultiplicationTable table = new MultiplicationTable(MultiplicationTableValue.Five);
            Multiplication multiplication = new Multiplication(table);
            MathProblem mathProblem = multiplication.GenerateMathProblem();
            int expectedSolution = mathProblem.LeftTerm * mathProblem.RightTerm;
            Assert.AreEqual(expectedSolution, mathProblem.Solution);
            Assert.AreEqual('*', mathProblem.OperationSign);
        }
    }
}