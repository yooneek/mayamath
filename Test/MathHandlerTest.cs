using System.Collections.Generic;
using MayaMath;
using MayaMath.CalculationType;
using MayaMath.Enum;
using NUnit.Framework;

namespace Test
{
    public class MathHandlerTest
    {
        [Test]
        public void TestGenerateMathProblems()
        {
            MultiplicationTable table5 = new MultiplicationTable(MultiplicationTableValue.Five);
            MultiplicationTable table6 = new MultiplicationTable(MultiplicationTableValue.Six);

            Division divisionTestA = new Division(table5);
            Division divisionTestB = new Division(table5);
            Division divisionTestC = new Division(table6);
            Multiplication multiplicationTestA = new Multiplication(table6);

            MathHandler mathHandler = new MathHandler();
            mathHandler.AddCalculationType(divisionTestA);
            mathHandler.AddCalculationType(divisionTestB);
            mathHandler.AddCalculationType(divisionTestC);
            mathHandler.AddCalculationType(multiplicationTestA);

            List<MathProblem> mathProblemSelection = mathHandler.GetRandomMathProblems(3);
            Assert.AreEqual(3, mathProblemSelection.Count);

            mathHandler.SolveMathProblem(mathProblemSelection[0], mathProblemSelection[0].Solution);
            Assert.AreEqual(2, mathProblemSelection.Count);

            mathProblemSelection = mathHandler.GetRandomMathProblems(3);
            Assert.AreEqual(3, mathProblemSelection.Count);
        }
    }
}